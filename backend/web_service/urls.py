"""web_service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from asistente import views as asistente
from evento import views as evento

from evento.viewsets import ConferenciaViewSet, TallerViewSet, PonenteViewSet, EventoViewSet, CarnetViewSet, ComentarioViewSet, PreguntaPonenteViewSet
from rest_framework import routers, serializers, viewsets







router = routers.DefaultRouter()
router.register(r'conferencia', ConferenciaViewSet)
router.register(r'taller', TallerViewSet)
router.register(r'ponente', PonenteViewSet)
router.register(r'evento', EventoViewSet)
router.register(r'carnet', CarnetViewSet)
router.register(r'comentario', ComentarioViewSet)
router.register(r'pregunta', PreguntaPonenteViewSet)

urlpatterns = [
	url(r'^alumnos.json', asistente.alumnos, name='alumnos'),
	url(r'cargar_alumnos', asistente.cargar_alumnos, name='cargar_alumnos'),
    url(r'api/comentar/$', evento.comentar, name='comentar'),
    url(r'api/preguntar/$', evento.preguntar, name='preguntar'),
    url(r'^', include(admin.site.urls)),

    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
