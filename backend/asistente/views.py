# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
from .models import Asistente
import os, csv, json, time


def alumnos(request):
	archivo = open(os.path.join(settings.BASE_DIR, 'alumnos.json'),'r+')
	return HttpResponse(archivo.read(), content_type='text/json')

def cargar_alumnos(request):
	alumnos = os.path.join(settings.BASE_DIR, 'alumnos.json')
	if request.method == 'POST':
		filee = request.FILES['archivo']
		jsonfile = open(alumnos,'w+')

		lines = filee.readlines()
		js = []
		for line in lines:
			line = unicode(line, "Latin-1")
			line = line.replace('"', '')
			line = line.replace('\r\n', '')
			line = line.split(',')

			js.append({ "value": line[0], "nombre": line[1], "carrera": line[2] })

		json.dump(js, jsonfile)

	archivo = open(alumnos,'r+')
	return render(request, 'asistente/cargar_alumnos.html', {'alumnos': archivo.read(), 'last': time.ctime(os.path.getmtime(alumnos))})