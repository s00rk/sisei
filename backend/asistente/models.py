from django.db import models


class Asistente(models.Model):
	no_control = models.CharField(max_length=10, blank=True, unique=True)
	nombre = models.CharField(max_length=200)
	apellido = models.CharField(max_length=200)
	telefono = models.CharField(max_length=10, blank=True)
	correo = models.EmailField(blank=True)
	carrera = models.CharField(max_length=100, blank=True)
	liquido = models.BooleanField(default=False, editable=False)
	fecha_liquido = models.DateTimeField(blank=True, null=True, editable=False)

	def __str__(self):
		return self.nombre
