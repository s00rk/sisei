from django.contrib import admin
from django.db.models import Sum

from .models import Asistente
from evento.models import Carnet
from pago.models import Pago, Compra
from super_inlines.admin import SuperInlineModelAdmin, SuperModelAdmin

class PagoInline(SuperInlineModelAdmin, admin.TabularInline):
	model = Pago
	extra = 1

class CompraInline(SuperInlineModelAdmin, admin.StackedInline):
    model = Compra
    extra = 1
    max_num = 1
    inlines = [PagoInline,]
    can_delete = False

@admin.register(Asistente)
class AsistenteAdmin(SuperModelAdmin):
	list_display = ('nombre', 'apellido', 'no_control', 'correo', 'Carnet', 'Pagado', 'liquido', 'fecha_liquido')
	search_fields = ('no_control', 'correo', 'nombre', 'apellido',)
	inlines = [CompraInline, ]

	def Pagado(self, obj):
		pagado = Pago.objects.filter(compra__asistente=obj.pk).aggregate(total=Sum('monto'))['total']
		return pagado

	def Carnet(self, obj):
		c = Compra.objects.get(asistente=obj.pk).carnet
		return "%s - $%s" % (c.nombre, c.precio)