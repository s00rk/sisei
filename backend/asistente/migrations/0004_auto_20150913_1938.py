# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('asistente', '0003_asistente_fecha_liquido'),
    ]

    operations = [
        migrations.CreateModel(
            name='HistoricalAsistente',
            fields=[
                ('id', models.IntegerField(verbose_name='ID', db_index=True, auto_created=True, blank=True)),
                ('no_control', models.CharField(max_length=10, blank=True)),
                ('nombre', models.CharField(max_length=200)),
                ('apellido', models.CharField(max_length=200)),
                ('telefono', models.CharField(max_length=10, blank=True)),
                ('correo', models.EmailField(max_length=254, blank=True)),
                ('carrera', models.CharField(max_length=100, blank=True)),
                ('liquido', models.BooleanField(default=False, editable=False)),
                ('fecha_liquido', models.DateTimeField(null=True, editable=False, blank=True)),
                ('history_id', models.AutoField(serialize=False, primary_key=True)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(max_length=1, choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')])),
                ('history_user', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
                'verbose_name': 'historical asistente',
            },
        ),
        migrations.AlterField(
            model_name='asistente',
            name='fecha_liquido',
            field=models.DateTimeField(null=True, editable=False, blank=True),
        ),
    ]
