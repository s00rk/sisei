# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('asistente', '0005_auto_20150913_1942'),
    ]

    operations = [
        migrations.AlterField(
            model_name='asistente',
            name='no_control',
            field=models.CharField(unique=True, max_length=10, blank=True),
        ),
    ]
