# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('asistente', '0002_asistente_liquido'),
    ]

    operations = [
        migrations.AddField(
            model_name='asistente',
            name='fecha_liquido',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
