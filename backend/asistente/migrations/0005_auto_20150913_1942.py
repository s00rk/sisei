# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('asistente', '0004_auto_20150913_1938'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='historicalasistente',
            name='history_user',
        ),
        migrations.DeleteModel(
            name='HistoricalAsistente',
        ),
    ]
