# -*- coding: utf-8 -*-
import csv, json
csvfile = open('alumnos2015.csv', 'r')
jsonfile = open('alumnos.json', 'w+')

lines = csvfile.readlines()
js = []
for line in lines:
	line = unicode(line, "Latin-1")
	line = line.replace('"', '')
	line = line.replace('\r\n', '')
	line = line.split(',')

	js.append({ "value": line[0], "nombre": line[1], "carrera": line[2] })

json.dump(js, jsonfile)