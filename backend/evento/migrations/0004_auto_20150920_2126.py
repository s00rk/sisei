# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('asistente', '0006_auto_20150920_2126'),
        ('evento', '0003_auto_20150913_1942'),
    ]

    operations = [
        migrations.CreateModel(
            name='TallerAsistente',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('asistente', models.OneToOneField(to='asistente.Asistente')),
            ],
            options={
                'verbose_name_plural': 'Taller - Asistente',
            },
        ),
        migrations.AlterModelOptions(
            name='cuartohotel',
            options={'verbose_name_plural': 'Hotel'},
        ),
        migrations.AddField(
            model_name='taller',
            name='asistentes',
            field=models.IntegerField(default=0, editable=False),
        ),
        migrations.AlterField(
            model_name='taller',
            name='imagen',
            field=models.ImageField(null=True, upload_to=b'talleres', blank=True),
        ),
        migrations.AddField(
            model_name='tallerasistente',
            name='taller',
            field=models.ForeignKey(to='evento.Taller'),
        ),
    ]
