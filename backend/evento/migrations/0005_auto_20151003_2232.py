# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('evento', '0004_auto_20150920_2126'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='carnet',
            options={'ordering': ['-evento']},
        ),
        migrations.AlterModelOptions(
            name='conferencia',
            options={'ordering': ['-evento']},
        ),
        migrations.AlterModelOptions(
            name='cuartohotel',
            options={'ordering': ['-evento']},
        ),
        migrations.AlterModelOptions(
            name='evento',
            options={'ordering': ['-fecha_inicio']},
        ),
        migrations.AlterModelOptions(
            name='taller',
            options={'ordering': ['-evento']},
        ),
        migrations.AlterModelOptions(
            name='tallerasistente',
            options={'ordering': ['-taller']},
        ),
        migrations.RemoveField(
            model_name='taller',
            name='asistentes',
        ),
        migrations.AlterField(
            model_name='carnet',
            name='evento',
            field=models.ForeignKey(default=1, to='evento.Evento'),
        ),
        migrations.AlterField(
            model_name='conferencia',
            name='evento',
            field=models.ForeignKey(default=1, to='evento.Evento'),
        ),
        migrations.AlterField(
            model_name='cuartohotel',
            name='evento',
            field=models.ForeignKey(default=1, to='evento.Evento'),
        ),
        migrations.AlterField(
            model_name='taller',
            name='evento',
            field=models.ForeignKey(default=1, to='evento.Evento'),
        ),
        migrations.AlterField(
            model_name='taller',
            name='lugar',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='taller',
            name='temario',
            field=models.TextField(null=True, blank=True),
        ),
    ]
