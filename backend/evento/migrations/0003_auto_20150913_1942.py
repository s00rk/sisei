# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('evento', '0002_historicalcarnet_historicalconferencia_historicalcuartoasistente_historicalcuartohotel_historicaleve'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='historicalcarnet',
            name='evento',
        ),
        migrations.RemoveField(
            model_name='historicalcarnet',
            name='history_user',
        ),
        migrations.RemoveField(
            model_name='historicalconferencia',
            name='evento',
        ),
        migrations.RemoveField(
            model_name='historicalconferencia',
            name='history_user',
        ),
        migrations.RemoveField(
            model_name='historicalconferencia',
            name='ponente',
        ),
        migrations.RemoveField(
            model_name='historicalcuartoasistente',
            name='asistente',
        ),
        migrations.RemoveField(
            model_name='historicalcuartoasistente',
            name='cuarto',
        ),
        migrations.RemoveField(
            model_name='historicalcuartoasistente',
            name='history_user',
        ),
        migrations.RemoveField(
            model_name='historicalcuartohotel',
            name='evento',
        ),
        migrations.RemoveField(
            model_name='historicalcuartohotel',
            name='history_user',
        ),
        migrations.RemoveField(
            model_name='historicalevento',
            name='history_user',
        ),
        migrations.RemoveField(
            model_name='historicalponente',
            name='history_user',
        ),
        migrations.RemoveField(
            model_name='historicaltaller',
            name='evento',
        ),
        migrations.RemoveField(
            model_name='historicaltaller',
            name='history_user',
        ),
        migrations.RemoveField(
            model_name='historicaltaller',
            name='ponente',
        ),
        migrations.DeleteModel(
            name='HistoricalCarnet',
        ),
        migrations.DeleteModel(
            name='HistoricalConferencia',
        ),
        migrations.DeleteModel(
            name='HistoricalCuartoAsistente',
        ),
        migrations.DeleteModel(
            name='HistoricalCuartoHotel',
        ),
        migrations.DeleteModel(
            name='HistoricalEvento',
        ),
        migrations.DeleteModel(
            name='HistoricalPonente',
        ),
        migrations.DeleteModel(
            name='HistoricalTaller',
        ),
    ]
