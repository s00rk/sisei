# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('asistente', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Carnet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=100)),
                ('descripcion', models.TextField(blank=True)),
                ('precio', models.DecimalField(max_digits=6, decimal_places=2)),
                ('viaje', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Conferencia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tema', models.CharField(max_length=100)),
                ('imagen', models.ImageField(null=True, upload_to=b'conferencias', blank=True)),
                ('descripcion', models.TextField()),
                ('fecha', models.DateField(null=True, blank=True)),
                ('hora', models.TimeField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='CuartoAsistente',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('asistente', models.ForeignKey(to='asistente.Asistente')),
            ],
            options={
                'verbose_name_plural': 'Cuartos',
            },
        ),
        migrations.CreateModel(
            name='CuartoHotel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'verbose_name_plural': 'Cuartos de Asistentes',
            },
        ),
        migrations.CreateModel(
            name='Evento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=50)),
                ('fecha_inicio', models.DateField()),
                ('fecha_final', models.DateField(null=True, blank=True)),
                ('lugar', models.TextField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Ponente',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=300)),
                ('imagen', models.ImageField(null=True, upload_to=b'ponentes', blank=True)),
                ('biografia', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Taller',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tema', models.CharField(max_length=100)),
                ('imagen', models.ImageField(upload_to=b'talleres')),
                ('temario', models.TextField()),
                ('capacidad', models.IntegerField(null=True, blank=True)),
                ('lugar', models.TextField()),
                ('fecha', models.DateField(null=True, blank=True)),
                ('hora', models.TimeField(null=True, blank=True)),
                ('evento', models.ForeignKey(to='evento.Evento')),
                ('ponente', models.ForeignKey(to='evento.Ponente')),
            ],
            options={
                'verbose_name_plural': 'Talleres',
            },
        ),
        migrations.AddField(
            model_name='cuartohotel',
            name='evento',
            field=models.ForeignKey(to='evento.Evento'),
        ),
        migrations.AddField(
            model_name='cuartoasistente',
            name='cuarto',
            field=models.ForeignKey(to='evento.CuartoHotel'),
        ),
        migrations.AddField(
            model_name='conferencia',
            name='evento',
            field=models.ForeignKey(to='evento.Evento'),
        ),
        migrations.AddField(
            model_name='conferencia',
            name='ponente',
            field=models.ForeignKey(to='evento.Ponente'),
        ),
        migrations.AddField(
            model_name='carnet',
            name='evento',
            field=models.ForeignKey(to='evento.Evento'),
        ),
    ]
