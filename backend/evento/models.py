from django.db import models
from asistente.models import Asistente

class Evento(models.Model):
	nombre = models.CharField(max_length=50)
	fecha_inicio = models.DateField()
	fecha_final = models.DateField(null=True, blank=True)
	lugar = models.TextField(blank=True)

	def __unicode__(self):
		return u'%s' % self.nombre

	class Meta:
		ordering = ['-fecha_inicio']

class Carnet(models.Model):
	evento = models.ForeignKey(Evento, default=Evento.objects.last().pk)
	nombre = models.CharField(max_length=100)
	descripcion = models.TextField(blank=True)
	precio = models.DecimalField(max_digits=6, decimal_places=2)
	viaje = models.BooleanField(default=False)

	def __unicode__(self):
		return u'%s' % self.nombre

	class Meta:
		ordering = ['-evento']

class Ponente(models.Model):
	nombre = models.CharField(max_length=300)
	imagen = models.ImageField(upload_to='ponentes', null=True, blank=True)
	biografia = models.TextField()

	def __unicode__(self):
		return u'%s' % self.nombre

class Taller(models.Model):
	ponente = models.ForeignKey(Ponente)
	evento = models.ForeignKey(Evento, default=Evento.objects.last().pk)
	tema = models.CharField(max_length=100)
	imagen = models.ImageField(upload_to='talleres', null=True, blank=True)
	temario = models.TextField(null=True, blank=True)
	capacidad = models.IntegerField(null=True, blank=True)
	lugar = models.TextField(null=True, blank=True)
	fecha = models.DateField(null=True, blank=True)
	hora = models.TimeField(null=True, blank=True)

	class Meta:
		verbose_name_plural = 'Talleres'

	def __unicode__(self):
		return u'%s' % self.tema

	class Meta:
		ordering = ['-evento']

class TallerAsistente(models.Model):
	asistente = models.OneToOneField(Asistente)
	taller = models.ForeignKey(Taller)

	class Meta:
		verbose_name_plural = 'Taller - Asistente'

	def __unicode__(self):
		return u"%s - %s" % (self.taller.tema, self.asistente.nombre)

	class Meta:
		ordering = ['-taller']

class Conferencia(models.Model):
	ponente = models.ForeignKey(Ponente)
	evento = models.ForeignKey(Evento, default=Evento.objects.last().pk)
	tema = models.CharField(max_length=100)
	imagen = models.ImageField(upload_to='conferencias', null=True, blank=True)
	descripcion = models.TextField()
	fecha = models.DateField(null=True, blank=True)
	hora = models.TimeField(null=True, blank=True)

	def __unicode__(self):
		return u'%s' % self.tema

	class Meta:
		ordering = ['-evento']

class CuartoHotel(models.Model):
	evento = models.ForeignKey(Evento, default=Evento.objects.last().pk)

	class Meta:
		verbose_name_plural = 'Hotel'

	def __unicode__(self):
		return "%s" % self.pk

	class Meta:
		ordering = ['-evento']

class CuartoAsistente(models.Model):
	cuarto = models.ForeignKey(CuartoHotel)
	asistente = models.ForeignKey(Asistente)

	class Meta:
		verbose_name_plural = 'Cuartos'

class Comentario(models.Model):
	evento = models.ForeignKey(Evento, default=Evento.objects.last().pk)
	mensaje = models.TextField()

	def __unicode__(self):
		return u'%s' % self.mensaje

class PreguntaPonente(models.Model):
	evento = models.ForeignKey(Evento, default=Evento.objects.last().pk)
	ponente = models.ForeignKey(Ponente)
	pregunta = models.TextField()

	def __unicode__(self):
		return u'%s - %s' % (self.ponente, self.pregunta)