from evento.models import Conferencia, Taller, Ponente, Carnet, Evento, PreguntaPonente, Comentario
from rest_framework import routers, serializers, viewsets

class EventoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Evento
class EventoViewSet(viewsets.ModelViewSet):
    queryset = Evento.objects.all()
    serializer_class = EventoSerializer

class PonenteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Ponente
class PonenteViewSet(viewsets.ModelViewSet):
    queryset = Ponente.objects.all()
    serializer_class = PonenteSerializer

class CarnetSerializer(serializers.HyperlinkedModelSerializer):
    evento = EventoSerializer()
    class Meta:
        model = Carnet
class CarnetViewSet(viewsets.ModelViewSet):
    queryset = Carnet.objects.all()
    serializer_class = CarnetSerializer

class ConferenciaSerializer(serializers.HyperlinkedModelSerializer):
    ponente = PonenteSerializer()
    evento = EventoSerializer()
    class Meta:
        model = Conferencia
class ConferenciaViewSet(viewsets.ModelViewSet):
    queryset = Conferencia.objects.all()
    serializer_class = ConferenciaSerializer

class TallerSerializer(serializers.HyperlinkedModelSerializer):
    ponente = PonenteSerializer()
    evento = EventoSerializer()
    class Meta:
        model = Taller
class TallerViewSet(viewsets.ModelViewSet):
    queryset = Taller.objects.all()
    serializer_class = TallerSerializer

class PreguntaPonenteSerializer(serializers.HyperlinkedModelSerializer):
    evento = EventoSerializer()
    ponente = PonenteSerializer()
    class Meta:
        model = PreguntaPonente
class PreguntaPonenteViewSet(viewsets.ModelViewSet):
    queryset = PreguntaPonente.objects.all()
    serializer_class = PreguntaPonenteSerializer

class ComentarioSerializer(serializers.HyperlinkedModelSerializer):
    evento = EventoSerializer()
    class Meta:
        model = Comentario
class ComentarioViewSet(viewsets.ModelViewSet):
    queryset = Comentario.objects.all()
    serializer_class = ComentarioSerializer