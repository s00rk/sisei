from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

from .models import Comentario, PreguntaPonente, Ponente
import json

@csrf_exempt
def comentar(request):
	resp = {'status': 'Failed'}
	if request.method == 'POST':
		if 'mensaje' in request.POST:
			c = Comentario()
			c.mensaje = request.POST['mensaje']
			c.save()
			resp = {'status': 'Ok'}
	return HttpResponse(json.dumps(resp), content_type='text/json')

@csrf_exempt
def preguntar(request):
	resp = {'status': 'Failed'}
	if request.method == 'POST':
		if 'ponente' in request.POST and 'pregunta' in request.POST:
			try:
				p = Ponente.objects.get(pk=request.POST['ponente'])
			except Ponente.DoesNotExists:
				p = None
			if p is not None:
				c = PreguntaPonente()		
				c.ponente = p
				c.pregunta = request.POST['pregunta']
				c.save()
				resp = {'status': 'Ok'}
	return HttpResponse(json.dumps(resp), content_type='text/json') 