from django.contrib import admin
from django.db.models import Q


from .models import Evento, Carnet, Ponente, Taller, Conferencia, CuartoAsistente, CuartoHotel, TallerAsistente, Comentario, PreguntaPonente

@admin.register(Evento)
class EventoAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'fecha_inicio', 'fecha_final',)
	search_fields = ('nombre', 'fecha_inicio',)

@admin.register(Carnet)
class CarnetAdmin(admin.ModelAdmin):
	list_display = ('evento', 'nombre', 'precio', 'viaje',)
	search_fields = ('nombre',)

@admin.register(Ponente)
class PonenteAdmin(admin.ModelAdmin):
	list_display = ('nombre',)
	search_fields = ('nombre',)

@admin.register(Taller)
class TallerAdmin(admin.ModelAdmin):
	list_display = ('ponente', 'evento', 'tema', 'fecha', 'hora', 'asistentes',)
	raw_id_fields = ('ponente',)
	search_fields = ('nombre', 'tema', 'evento',)

	def asistentes(self, obj):
		return TallerAsistente.objects.filter(taller=obj.pk).count()

@admin.register(TallerAsistente)
class TallerAsistenteAdmin(admin.ModelAdmin):
	list_display = ('taller', 'asistente',)
	search_fields = ('asistente__nombre', 'taller__tema', 'asistente__apellido', 'asistente__no_control')
	raw_id_fields = ('asistente',)

@admin.register(Conferencia)
class ConferenciaAdmin(admin.ModelAdmin):
	list_display = ('ponente', 'evento', 'tema', 'fecha', 'hora',)
	raw_id_fields = ('ponente',)
	search_fields = ('ponente', 'tema', 'evento',)

@admin.register(CuartoAsistente)
class CuartoAsistenteAdmin(admin.ModelAdmin):
	list_display = ('cuarto', 'asistente',)
	raw_id_fields = ('asistente',)
	search_fields = ('asistente',)

class CuartoInline(admin.TabularInline):
	model = CuartoAsistente
	extra = 1
	raw_id_fields = ('asistente',)

@admin.register(CuartoHotel)
class CuartoHotelAdmin(admin.ModelAdmin):
	list_display = ('cuarto', 'evento', 'cantidad')
	inlines = [CuartoInline,]
	search_fields = ('evento__nombre',)

	def get_search_results(self, request, queryset, search_term):
		queryset, use_distinct = super(CuartoHotelAdmin, self).get_search_results(request, queryset, search_term)
		print use_distinct
		p = CuartoAsistente.objects.filter(Q(asistente__nombre__icontains=search_term) | Q(asistente__apellido__icontains=search_term) | Q(asistente__no_control=search_term))
		c = []
		for pp in p:
			c.append(pp.cuarto.pk)
		if len(c) > 0:
			queryset |= self.model.objects.filter(pk__in=c)
		return queryset, use_distinct

	def cantidad(self, obj):
		return CuartoAsistente.objects.filter(cuarto=obj.pk).count()

	def cuarto(self, obj):
		return obj.pk

@admin.register(Comentario)
class ComentarioAdmin(admin.ModelAdmin):
	list_display = ('evento', 'mensaje',)
	search_fields = ('evento__nombre', 'mensaje',)
	raw_id_fields = ('evento',)

@admin.register(PreguntaPonente)
class PreguntaPonenteAdmin(admin.ModelAdmin):
	list_display = ('evento', 'ponente', 'pregunta',)
	search_fields = ('evento__nombre', 'ponente__nombre', 'pregunta',)
	raw_id_fields = ('evento', 'ponente',)