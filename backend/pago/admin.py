from django.contrib import admin

from .models import Compra, Pago

@admin.register(Compra)
class CompraAdmin(admin.ModelAdmin):
	list_display = ('carnet', 'asistente',)
	raw_id_fields = ('carnet', 'asistente',)

@admin.register(Pago)
class PagoAdmin(admin.ModelAdmin):
	list_display = ('compra', 'monto', 'fecha',)
	raw_id_fields = ('compra',)