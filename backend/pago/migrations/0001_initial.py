# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('asistente', '0001_initial'),
        ('evento', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Compra',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('asistente', models.ForeignKey(to='asistente.Asistente')),
                ('carnet', models.ForeignKey(to='evento.Carnet')),
            ],
        ),
        migrations.CreateModel(
            name='Pago',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('monto', models.DecimalField(max_digits=6, decimal_places=2)),
                ('fecha', models.DateTimeField(auto_now_add=True)),
                ('compra', models.ForeignKey(to='pago.Compra')),
            ],
        ),
    ]
