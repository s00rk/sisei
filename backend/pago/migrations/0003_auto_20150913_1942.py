# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pago', '0002_historicalcompra_historicalpago'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='historicalcompra',
            name='asistente',
        ),
        migrations.RemoveField(
            model_name='historicalcompra',
            name='carnet',
        ),
        migrations.RemoveField(
            model_name='historicalcompra',
            name='history_user',
        ),
        migrations.RemoveField(
            model_name='historicalpago',
            name='compra',
        ),
        migrations.RemoveField(
            model_name='historicalpago',
            name='history_user',
        ),
        migrations.DeleteModel(
            name='HistoricalCompra',
        ),
        migrations.DeleteModel(
            name='HistoricalPago',
        ),
    ]
