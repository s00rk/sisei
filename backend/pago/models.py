from django.db import models
from django.db.models import Sum
from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete
from django.core.exceptions import ValidationError

from evento.models import Carnet
from asistente.models import Asistente
from datetime import datetime

class Compra(models.Model):
	carnet = models.ForeignKey(Carnet)
	asistente = models.ForeignKey(Asistente)

	def __str__(self):
		return "%s - %s" % (self.carnet, self.asistente)

class Pago(models.Model):
	compra = models.ForeignKey(Compra)
	monto = models.DecimalField(max_digits=6, decimal_places=2)
	fecha = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return "%s - %s" % (self.compra.asistente, self.monto)

	def clean(self):
		if self.pk is None:
			err = False
			try:
				asist = self.compra.asistente
				pagado = Pago.objects.filter(compra=self.compra).aggregate(total=Sum('monto'))['total']
				total = self.compra.carnet.precio
				if pagado is None:
					pagado = 0
				if (pagado + self.monto) > total:
					err = True
			except:
				pass
			if err:
				raise ValidationError('El pago sobrepasa el precio del carnet.')

@receiver(post_delete, sender=Pago)
def PagoDelete(sender, instance, using, **kwards):
	asist = instance.compra.asistente
	pagado = Pago.objects.filter(compra=instance.compra).aggregate(total=Sum('monto'))['total']
	total = instance.compra.carnet.precio
	if pagado is None:
		pagado = 0
	if pagado >= total:
		asist.liquido = True
		asist.fecha_liquido = datetime.today()
		asist.save()
	else: 
		asist.liquido = False
		asist.fecha_liquido = None
		asist.save()

@receiver(post_save, sender=Pago)
def PagoSave(sender, instance, created, raw, using, update_fields, **kwards):
	asist = instance.compra.asistente
 	pagado = Pago.objects.filter(compra=instance.compra).aggregate(total=Sum('monto'))['total']
 	total = instance.compra.carnet.precio
 	if pagado is None:
		pagado = 0
 	if pagado >= total:
 		asist.liquido = True
 		asist.fecha_liquido = datetime.today()
 		asist.save()
 	else: 
		asist.liquido = False
		asist.fecha_liquido = None
		asist.save()