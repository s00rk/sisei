-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-09-2015 a las 21:42:13
-- Versión del servidor: 5.6.26
-- Versión de PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sisei`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `abonos`
--

CREATE TABLE IF NOT EXISTS `abonos` (
  `id` int(11) NOT NULL,
  `asistente` int(11) NOT NULL,
  `vendedor` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `abono` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `abonos`
--

INSERT INTO `abonos` (`id`, `asistente`, `vendedor`, `fecha`, `abono`) VALUES
(8, 1, 'Martin', '2015-09-07 18:27:09', 1500),
(10, 3, 'Hector Murillo', '2015-09-07 19:51:14', 100),
(11, 4, 'Hector Murillo', '2015-09-07 19:53:30', 100),
(12, 5, 'Hector Murillo', '2015-09-07 21:05:39', 100),
(13, 6, 'Anel Piña', '2015-09-07 21:08:53', 100),
(14, 7, 'Anel Piña', '2015-09-07 22:22:28', 100),
(15, 8, 'Anel Piña', '2015-09-08 20:42:37', 100),
(16, 9, '0', '2015-09-08 22:55:00', 100),
(17, 10, 'Anel Piña', '2015-09-09 20:14:01', 1500),
(18, 11, 'Anel Piña', '2015-09-09 20:16:32', 100),
(19, 12, 'Anel Piña', '2015-09-09 20:24:35', 100),
(20, 13, 'Anel Piña', '2015-09-09 21:24:28', 100),
(21, 14, 'Anel Piña', '2015-09-09 21:25:53', 100),
(22, 15, 'Anel Piña', '2015-09-09 21:27:08', 100),
(23, 16, 'Anel Piña', '2015-09-09 21:28:12', 100),
(24, 17, 'Anel Piña', '2015-09-09 21:45:40', 100);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistentes`
--

CREATE TABLE IF NOT EXISTS `asistentes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(300) COLLATE utf8_spanish2_ci NOT NULL,
  `email` varchar(300) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `escuela` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `carrera` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `matricula` varchar(8) COLLATE utf8_spanish2_ci NOT NULL,
  `liquido` tinyint(1) NOT NULL,
  `abono` int(11) NOT NULL,
  `costeCarnet` int(11) NOT NULL,
  `taller` int(11) NOT NULL,
  `cuarto` int(11) NOT NULL,
  `numero_liquidado` int(11) NOT NULL,
  `carnet` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `asistentes`
--

INSERT INTO `asistentes` (`id`, `nombre`, `email`, `telefono`, `escuela`, `carrera`, `matricula`, `liquido`, `abono`, `costeCarnet`, `taller`, `cuarto`, `numero_liquidado`, `carnet`) VALUES
(1, 'martin vazquez', 'martincito@gmail.com', '6671234321', 'Instituto tecnologico de culiacan.', 'sistema', '12170707', 1, 1500, 1500, 0, 0, 1, 'Completo'),
(3, 'Nieto Murillo Diego Alejandro', 'diegonietomurillo@gmail.com', '6674737411', 'Instituto tecnologico de culiacan.', 'Sistemas', '12170111', 0, 100, 1500, 0, 0, 0, 'Completo'),
(4, 'Medina Montoya  Georgina Guadalupe', 'georgina_g_m_m@hotmail.com', '6674789595', 'Instituto tecnologico de culiacan.', 'Sistemas', '12170638', 0, 100, 1500, 0, 0, 0, 'Completo'),
(5, 'Manuel Alonso Madrigal Alcaraz', 'malonso@hotmail.com', '6671896936', 'Instituto tecnologico de culiacan.', 'Tic''s', '13170784', 0, 100, 1500, 0, 0, 0, 'Completo'),
(6, 'Jose Luis Osuna Picos', 'mtro_Jose@hotmail.com', '6671451960', 'Instituto tecnologico de culiacan.', 'Sistemas', '12170440', 0, 100, 1500, 0, 0, 0, 'Completo'),
(7, 'Perez Urvina Jovany', 'compu_64@hotmail.com', '6671665872', 'Instituto tecnologico de culiacan.', 'Sistemas', '13170330', 0, 100, 1100, 0, 0, 0, 'Basico'),
(8, 'Carlos Antonio Sanchez Guzman', 'carlos_anto9519@hotmail.com', '6677862778', 'Instituto tecnologico de culiacan.', 'Sistemas', '13170354', 0, 100, 1500, 0, 0, 0, 'Completo'),
(9, 'José Ramón Esquerra Ibarra', 'joseesquerra12@hotmail.com', '6971119030', 'Instituto tecnologico de culiacan.', 'Ing. Sistemas', '15171116', 0, 100, 1500, 0, 0, 0, 'Completo'),
(10, 'Luis Fernando Sapiens Ochoa', 'luisfer_8@live.com', '6671898822', 'Instituto tecnologico de culiacan.', 'Sistemas', '12170691', 1, 1500, 1500, 0, 0, 2, 'Completo'),
(11, 'Omar Ramses Ramos Esquerra', 'maik_syp@hotmail.com', '6671277054', 'Instituto tecnologico de culiacan.', 'Sistemas', '12170116', 0, 100, 1500, 0, 0, 0, 'Completo'),
(12, 'Raúl Humberto Cardenas Baez', 'rulocardenas94@gmail.com', '6677953854', 'Instituto tecnologico de culiacan.', 'Sistemas', '12170566', 0, 100, 1500, 0, 0, 0, 'Completo'),
(13, 'Monica Lizbeth Ramirez López', 'ramirezm50@hotmail.com', '6673257740', 'Instituto tecnologico de culiacan.', 'Sistemas', '12170669', 0, 100, 1100, 0, 0, 0, 'Basico'),
(14, 'Enrrique Sanchez Angulo', 'enri123415@gmail.com', '6671910736', 'Instituto tecnologico de culiacan.', 'sistemas', '15171208', 0, 100, 1100, 0, 0, 0, 'Basico'),
(15, 'Jesus Leonel López León', 'leonel_121996@hotmail.com', '6677848392', 'Instituto tecnologico de culiacan.', 'Sistemas', '14170922', 0, 100, 1500, 0, 0, 0, 'Completo'),
(16, 'Cristal Anahi Rubio Valenzuela', 'anahi_rv18@hotmail.com', '6672325505', 'Instituto tecnologico de culiacan.', 'Sistemas', '14170991', 0, 100, 1500, 0, 0, 0, 'Completo'),
(17, 'Francisco Javier Erenas López', 'fco_cromo@hotmail.com', '6679956183', 'Instituto tecnologico de culiacan.', 'Sistemas', '12170099', 0, 100, 1500, 0, 0, 0, 'Completo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carnets`
--

CREATE TABLE IF NOT EXISTS `carnets` (
  `nombre` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `precio` int(11) NOT NULL,
  `color` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `carnets`
--

INSERT INTO `carnets` (`nombre`, `precio`, `color`, `id`) VALUES
('Basico', 1100, 'azul', 1),
('Completo', 1500, 'verde', 2),
('Foraneo', 2400, 'rosa', 3),
('Regalo', 0, 'amarillo', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias_blog`
--

CREATE TABLE IF NOT EXISTS `categorias_blog` (
  `categoria` varchar(30) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE IF NOT EXISTS `comentarios` (
  `id` int(11) NOT NULL,
  `post` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `comentario` text COLLATE utf8_spanish2_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `email` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `aprobado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuartos`
--

CREATE TABLE IF NOT EXISTS `cuartos` (
  `id` int(11) NOT NULL,
  `encargado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE IF NOT EXISTS `permisos` (
  `usuario` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `root` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`usuario`, `root`) VALUES
('Daniela', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `nombre` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `url` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `contenido` text COLLATE utf8_spanish2_ci NOT NULL,
  `imagen` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish2_ci NOT NULL,
  `autor` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `publicado` tinyint(1) NOT NULL,
  `fecha` date NOT NULL,
  `categoria` varchar(200) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registros`
--

CREATE TABLE IF NOT EXISTS `registros` (
  `id` int(11) NOT NULL,
  `usuario` varchar(35) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `tipo` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(200) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `registros`
--

INSERT INTO `registros` (`id`, `usuario`, `fecha`, `tipo`, `descripcion`) VALUES
(1, '925d7518fc597af0e43f5606f9a51512', '2015-09-07 18:10:07', 'login', 'Sesion iniciada desde: Unknown Windows OS/Chrome'),
(2, '925d7518fc597af0e43f5606f9a51512', '2015-09-07 18:14:34', 'login', 'Sesion iniciada desde: Unknown Windows OS/Chrome'),
(3, '925d7518fc597af0e43f5606f9a51512', '2015-09-07 18:18:28', 'administrar usuarios', 'Se ha registrado a: Daniela'),
(4, '826acedae5686466ac4b3ccfec725ac0', '2015-09-07 18:22:42', 'login', 'Sesion iniciada desde: Unknown Windows OS/Chrome'),
(5, '826acedae5686466ac4b3ccfec725ac0', '2015-09-07 18:23:10', 'administrar usuarios', 'Se ha registrado a: Martin'),
(6, '81d6f316d169150d0e8733866c38684d', '2015-09-07 18:24:34', 'login', 'Sesion iniciada desde: Unknown Windows OS/Chrome'),
(7, '81d6f316d169150d0e8733866c38684d', '2015-09-07 18:26:27', 'login', 'Sesion iniciada desde: Unknown Windows OS/Chrome'),
(8, 'Martin', '2015-09-07 18:27:09', 'Taller', 'El usuario martin vazquez ha sido registrado.'),
(9, '826acedae5686466ac4b3ccfec725ac0', '2015-09-07 18:32:55', 'login', 'Sesion iniciada desde: Unknown Windows OS/Chrome'),
(10, '81d6f316d169150d0e8733866c38684d', '2015-09-07 18:35:58', 'login', 'Sesion iniciada desde: Unknown Windows OS/Chrome'),
(11, 'Martin', '2015-09-07 18:36:53', 'Taller', 'El usuario Ramirez Loaiza Jorge Eduardo ha sido registrado.'),
(12, '826acedae5686466ac4b3ccfec725ac0', '2015-09-07 18:37:04', 'login', 'Sesion iniciada desde: Unknown Windows OS/Chrome'),
(13, '826acedae5686466ac4b3ccfec725ac0', '2015-09-07 19:32:12', 'login', 'Sesion iniciada desde: Unknown Windows OS/Chrome'),
(14, '826acedae5686466ac4b3ccfec725ac0', '2015-09-07 19:33:06', 'administrar usuarios', 'Se ha registrado a: Hector Murillo'),
(15, '24b49dfcf6c8b1d1b317f809f1ed9c36', '2015-09-07 19:38:04', 'login', 'Sesion iniciada desde: Unknown Windows OS/Chrome'),
(16, 'Hector Murillo', '2015-09-07 19:51:14', 'Taller', 'El usuario Nieto Murillo Diego Alejandro ha sido registrado.'),
(17, 'Hector Murillo', '2015-09-07 19:53:30', 'Taller', 'El usuario Medina Montoya  Georgina Guadalupe ha sido registrado.'),
(18, 'Hector Murillo', '2015-09-07 21:05:39', 'Taller', 'El usuario Manuel Alonso Madrigal Alcaraz ha sido registrado.'),
(19, '826acedae5686466ac4b3ccfec725ac0', '2015-09-07 21:07:01', 'login', 'Sesion iniciada desde: Unknown Windows OS/Chrome'),
(20, '826acedae5686466ac4b3ccfec725ac0', '2015-09-07 21:07:30', 'administrar usuarios', 'Se ha registrado a: Anel Piña'),
(21, '64d38fdb5a739b6b3f03cb4385212a1e', '2015-09-07 21:07:49', 'login', 'Sesion iniciada desde: Unknown Windows OS/Chrome'),
(22, 'Anel Piña', '2015-09-07 21:08:53', 'Taller', 'El usuario Jose Luis Osuna Picos ha sido registrado.'),
(23, 'Anel Piña', '2015-09-07 22:22:28', 'Taller', 'El usuario Perez Urvina Jovany ha sido registrado.'),
(24, 'Anel Piña', '2015-09-08 20:42:37', 'Taller', 'El usuario Carlos Antonio Sanchez Guzman ha sido registrado.'),
(25, '0', '2015-09-08 22:55:00', 'Taller', 'El usuario José Ramón Esquerra Ibarra ha sido registrado.'),
(26, 'Anel Piña', '2015-09-09 20:14:01', 'Taller', 'El usuario Luis Fernando Sapiens Ochoa ha sido registrado.'),
(27, 'Anel Piña', '2015-09-09 20:16:32', 'Taller', 'El usuario Omar Ramses Ramos Esquerra ha sido registrado.'),
(28, 'Anel Piña', '2015-09-09 20:24:35', 'Taller', 'El usuario Raúl Humberto Cardenas Baez ha sido registrado.'),
(29, 'Anel Piña', '2015-09-09 21:24:28', 'Taller', 'El usuario Monica Lizbeth Ramirez López ha sido registrado.'),
(30, 'Anel Piña', '2015-09-09 21:25:53', 'Taller', 'El usuario Enrrique Sanchez Angulo ha sido registrado.'),
(31, 'Anel Piña', '2015-09-09 21:27:08', 'Taller', 'El usuario Jesus Leonel López León ha sido registrado.'),
(32, 'Anel Piña', '2015-09-09 21:28:12', 'Taller', 'El usuario Cristal Anahi Rubio Valenzuela ha sido registrado.'),
(33, 'Anel Piña', '2015-09-09 21:45:40', 'Taller', 'El usuario Francisco Javier Erenas López ha sido registrado.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seguridad_login`
--

CREATE TABLE IF NOT EXISTS `seguridad_login` (
  `usuario` varchar(35) COLLATE utf8_spanish2_ci NOT NULL,
  `ip` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `intentos` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesion`
--

CREATE TABLE IF NOT EXISTS `sesion` (
  `usuario` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `ip` varchar(15) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `sesion`
--

INSERT INTO `sesion` (`usuario`, `ip`) VALUES
('Anel Piña', '::1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `taller`
--

CREATE TABLE IF NOT EXISTS `taller` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `cupo` int(11) NOT NULL,
  `descripcion` text COLLATE utf8_spanish2_ci NOT NULL,
  `tallerista` varchar(250) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `clave` varchar(35) COLLATE utf8_spanish2_ci NOT NULL,
  `skey` varchar(8) COLLATE utf8_spanish2_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `limitar_intentos` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`nombre`, `clave`, `skey`, `email`, `limitar_intentos`) VALUES
('Anel Piña', 'e10adc3949ba59abbe56e057f20f883e', '', 'anel_p.borrego@hotmail.com', 0),
('Daniela', '1bc88e4a490f67454760554b2ce7c6d4', '', 'danielabojorquez9@gmail.com', 0),
('Hector Murillo', 'e10adc3949ba59abbe56e057f20f883e', '', 'c21.hector@gmail.com', 0),
('Martin', '34f74c049edea51851c6924f4a386762', '', 'martin_vazqez@hotmail.com', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `abonos`
--
ALTER TABLE `abonos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `asistentes`
--
ALTER TABLE `asistentes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `carnets`
--
ALTER TABLE `carnets`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cuartos`
--
ALTER TABLE `cuartos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`usuario`);

--
-- Indices de la tabla `registros`
--
ALTER TABLE `registros`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `seguridad_login`
--
ALTER TABLE `seguridad_login`
  ADD PRIMARY KEY (`ip`,`usuario`);

--
-- Indices de la tabla `sesion`
--
ALTER TABLE `sesion`
  ADD PRIMARY KEY (`usuario`,`ip`);

--
-- Indices de la tabla `taller`
--
ALTER TABLE `taller`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`nombre`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `abonos`
--
ALTER TABLE `abonos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `asistentes`
--
ALTER TABLE `asistentes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `carnets`
--
ALTER TABLE `carnets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `cuartos`
--
ALTER TABLE `cuartos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `registros`
--
ALTER TABLE `registros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `taller`
--
ALTER TABLE `taller`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD CONSTRAINT `permisos_ibfk_1` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`nombre`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
