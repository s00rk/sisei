var Talleres = function(){
	$.get('http://localhost:8000/api/taller/?format=json', function(data){
		var lista = $('.workshop-nav');
		var lista2 = $('#workshop .tab-content');
		var txt = '';
		var txt2 = '';
		
		$.each(data, function(i, taller){
			if(i == 0){
				txt += '<li class="active"><a href="#workshop-' + i + '" data-toggle="tab">' + taller.tema + '</a></li>';
				txt2 += '<div role="tabpanel" class="tab-pane fade in active" id="workshop-' + i + '"><div class="thumbnail"><figure class="image"><img src="' + taller.imagen + '" alt="image"></figure><div class="caption"><h4>' + taller.tema + '</h4><p>' + taller.descripcion + '</p></div></div></div>';
			}else{
				txt += '<li><a href="#workshop-' + i + '" data-toggle="tab">' + taller.tema + '</a></li>';
				txt2 = '<div role="tabpanel" class="tab-pane fade" id="workshop-' + i + '"><div class="thumbnail"><figure class="image"><img src="' + taller.imagen + '" alt="image"></figure><div class="caption"><h4>' + taller.tema + '</h4><p>' + taller.temario + '</p></div></div></div>';
			}
			
		});
		lista.html(txt);
		lista2.html(txt2);
		console.log(data);
	});
};

$(document).on('ready', function(){
	Talleres();
});