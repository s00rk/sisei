/*!
 * 
 * Workshop Event Management HTML5 Template v.1.0.0
 * 
 */



 jQuery(function($){
 	"use strict";

 	/***Color Panel*/
 	$('.show-panel').on('click', this, function () {
 		$('.colorpanel').slideToggle(500);
 	});
 	$('.colorpanel .colours-wrapper .entry').on('click', this, function(){
 		var newColour = $(this).attr('data-colour');
 		if($(this).hasClass('active')) return false;
 		$(this).parent().find('.active').removeClass('active');
 		$(this).addClass('active');
 		$('body').removeClass('green blue purple orange red').addClass(newColour);
 	});

	/*** Navigation in responsive layouts 
	--------------------------------------------------- ****/
	$('.top-nav-bar-2').clone(true).appendTo('body').addClass('nav-tablet').removeClass('top-nav-bar');

	
	/***** Main Menu for Single page 
	-------------------------------------------------------------- ***/
	function singlePageMenu(){
		if ( $('.single-page').length) {

			$('.main-nav').each(function(){
				
				var $active, $content, $links = $(this).find('a.on'),
				$li = $(this).find('a').closest('li');
				
				$active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
				$content = $($active.attr('href'));
				
				$(this).on('click', 'a', function(e){
					
					$li.removeClass('active');
					
					$active = $(this);
					$content = $($(this).attr('href'));
					
					$(this).closest('li').addClass('active');
					$("body,html").animate({scrollTop:$content.position().top + 1}, 1000);
					
					e.preventDefault();
				});
			});

		}
	}	/*** /singlePageMenu ***/

	/*** single page Main Menu add Class to current Menu's Target Section ****/
	function activeMainMenuItem(){

		var scrollPos = $(document).scrollTop();

		if ( $('.single-page').length) {
			$('.main-nav a.on').each(function () {
				var currLink = $(this),
				refElement = $(currLink.attr("href")),
				$li = currLink.closest('li');
				
				if ( refElement.position().top <= scrollPos && (refElement.position().top - 2) + refElement.outerHeight() > scrollPos) {
					$('.main-nav > li').removeClass("active");
					$li.addClass("active");
				}
				else{
					$li.removeClass("active");
				}
			});
		}
	}	/*** /activeMainMenuItem ***/	

	/**** Create Fixed on page scroll ***/
	function scrolledHeader(){
		if ( $('.sticky-menu-trigger').length ) {
			var menuTrigger = $('.sticky-menu-trigger').outerHeight();
			if ( $(document).scrollTop() > menuTrigger){
				$('body').addClass('scrolled');
			}
			else {
				$('body').removeClass('scrolled');
			}
		}
	}


	/***** Main Menu for Single page 
	-------------------------------------------------------------- ***/
	function triggerToSection(){

		$('.triggerToSection').on('click', this, function(e){

			var targetSection = $(this).attr('href');
			$("body,html").animate({scrollTop:$(targetSection).position().top + 1}, 1000);
			e.preventDefault();
		});

	}	/*** /singlePageMenu ***/


	singlePageMenu();
	activeMainMenuItem();
	scrolledHeader();
	triggerToSection();

	$(window).scroll( function(){
		activeMainMenuItem();
		scrolledHeader();
	});

	$('.trigger-menu').on('click', this, function(){
		$('body').toggleClass('menu-opened');
	});

	$('.trigger-menu-2').on('click', this, function(){
		$('body').toggleClass('menu-opened-2');
	});

	$('.overlay, .top-nav-bar li, .nav-tablet li').on('click', this, function(){
		$('body').removeClass('menu-opened menu-opened-2');
	});

	/*** Collapse Accordions ***/	 
	if ( $('.panel-group').length) {
		$('.panel-group').collapse();

		$('.panel-collapse').on('show.bs.collapse',function(){
			$(this).prev('.panel-heading').closest('.panel').addClass("panel-active");
		});
		$('.panel-collapse').on('hide.bs.collapse',function(){
			$(this).prev('.panel-heading').closest('.panel').removeClass("panel-active");
		});

	}


	/*** Tweets carousel ***/
	if ( $('.slider-list').length) {
		var sList = $(".slider-list");
		
		sList.owlCarousel({
			autoPlay: true,
			singleItem: true,
			pagination: false
		});
	}

	/*** owl carousel ***/
	if ( $('.carousel-single').length) {
		var cSingle = $(".carousel-single");
		
		cSingle.owlCarousel({
			autoPlay: false,
			singleItem: true,
			pagination: false
		});

		$(".caro-next").on('click', this, function(){
			cSingle.trigger('owl.next');
		});
		$(".caro-prev").on('click', this, function(){
			cSingle.trigger('owl.prev');
		});
	}

	/*** Tweets carousel ***/
	if ( $('.carousel-tweets').length) {
		var cTweets = $(".carousel-tweets");
		
		cTweets.owlCarousel({
			autoPlay: true,
			singleItem: true,
			pagination: false,
			transitionStyle: "fade"
		});
	}

	/**** Sync owl-carousel ****/
	function carouselThumb(){
		
		var sync1 = $("#sync1");
		var sync2 = $("#sync2");
		
		sync1.owlCarousel({
			singleItem : true,
			slideSpeed : 1000,
			navigation: false,
			pagination:false,
			afterAction : syncPosition,
			responsiveRefreshRate : 200
		});
		sync2.owlCarousel({
			items : 4,
			itemsDesktop      : [1199,4],
			itemsDesktopSmall     : [979,4],
			itemsTablet       : [768,2],
			itemsMobile       : [479,2],
			navigation: false,
			pagination:false,
			responsiveRefreshRate : 100,
			afterInit : function(el){
				el.find(".owl-item").eq(0).addClass("synced");
			}
		});
		function syncPosition(el){
			var current = this.currentItem;
			$("#sync2")
			.find(".owl-item")
			.removeClass("synced")
			.eq(current)
			.addClass("synced")
			if($("#sync2").data("owlCarousel") !== undefined){
				center(current);
			}
		}
		
		$("#sync2").on("click", ".owl-item", function(e){
			e.preventDefault();
			var number = $(this).data("owlItem");
			sync1.trigger("owl.goTo",number);
		});
		
		function center(number){
			var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
			
			var num = number;
			var found = false;
			for(var i in sync2visible){
				if(num === sync2visible[i]){
					var found = true;
				}
			}
			
			if(found===false){
				if(num>sync2visible[sync2visible.length-1]){
					sync2.trigger("owl.goTo", num - sync2visible.length+2)
				}else{
					if(num - 1 === -1){
						num = 0;
					}
					sync2.trigger("owl.goTo", num);
				}
			} else if(num === sync2visible[sync2visible.length-1]){
				sync2.trigger("owl.goTo", sync2visible[1]);
			} else if(num === sync2visible[0]){
				sync2.trigger("owl.goTo", num-1);
			}
		}
		
	}

	if ( $("#sync2").length ) {
		carouselThumb();
	}

	/*** partners carousel ***/
	if ( $('.carousel-partners').length) {
		var cPartner = $(".carousel-partners");
		cPartner.owlCarousel({
			autoPlay: true,
			items : 5,
			itemsDesktop      : [1199,4],
			itemsDesktopSmall     : [979,3],
			itemsTablet       : [768,2],
			itemsMobile       : [479,1],
			pagination: false,
			navigation: false,
			responsiveRefreshRate : 100
		});
	}

	/*** Scroll Bars  ***/
	if ( $('.scrolling-area').length ) {
		$('.scrolling-area').mCustomScrollbar({
			autoHideScrollbar:true,
			theme:"minimal-dark",
			scrollInertia:200
		});
	}

	/*** Select Package Text ***/
	$('.pricing-table-box').on('click', this, function(){
		var packg = $(this).data('package');
		console.log(packg);
		$('#select-package').text(packg);
		$('.pricing-table-box').not(this).removeClass('selected');
		$(this).addClass('selected');
	});


	/*** Load more blog posts ***/
	$('a#load-post').on('click', function(e){
		
		$.ajax({
			url : 'load-blog.html',
			data : '',
			dataType : 'html',
			success: function(data){
				var newhtml = $.parseHTML(data);
				var li = $(newhtml);
				$(li).each( function( k, v){
					$(v).hide();
					$('ul.blog-list').append(v).delay(2000).children().slideDown('slow');
				});
			}
		});

		e.preventDefault();
	});


	/*** Play and Pause videos in carousel ****/
	$('.video-button-play').on('click', this, function(){

		$(this).siblings('video')[0].play();
		var elem = $(this).siblings('video')[0];
		if (elem.requestFullscreen) {
			elem.requestFullscreen();
		} else if (elem.mozRequestFullScreen) {
			elem.mozRequestFullScreen();
		} else if (elem.webkitRequestFullscreen) {
			elem.webkitRequestFullscreen();
		}
		$(this).closest('.item').addClass('active').removeClass('paused-vid');

	});
	$('.video-button-pause').on('click', this, function(){

		$(this).siblings('video')[0].pause();
		$(this).closest('.item').addClass('paused-vid');

	});
	$('#sync2 .item').on('click', this, function(){
		$('#sync1 .item video').each(function(){
			$(this).get(0).pause();
			$(this).closest('.item').addClass('paused-vid');
		});
	});

});