<?php
	if(isset($_POST))
	{
		$captcha = $_POST['g-recaptcha-response'];
		$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LeX2gwTAAAAAAnHLWJ40PkYSQui6yRsAYhsfW1_&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
		
		if(strpos($response,'true') !== false && strlen($_POST['email']) > 0 && strlen($_POST['message']) > 0 && strlen($_POST['name']) > 0)
		{
			$to = 'victor.riverac92@gmail.com';
			$subject = 'SISeI Contacto - ' . $_POST['name'];
			$message = $_POST['message'];
			$headers = "From: " . $_POST['email'] . "\r\n";
			mail($to, $subject, $message, $headers);
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>SISeI 9</title>

	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/owl_carousel.css" rel="stylesheet">
	<link href="css/jquery_customscrollbar.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<link href="css/color.css" rel="stylesheet">
	<link href="css/panel.css" rel="stylesheet" type="text/css"/>

	<!--[if lt IE 9]-->
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<!--[endif]-->

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

	<link rel="shortcut icon" href="images/icon.png">
</head>
<body class="single-page blue">

	<div class="pageWrap">

		<div id="home">
			<div class="small-tpBar small-tpBar-2">
				<div class="logo">
					<a href="/"><img src="images/logo.png" width="16%" style="position: absolute;"></a>
				</div>

				<span class="trigger-menu-2">
					<span class="button-lines"></span>
					<span class="button-lines"></span>
					<span class="button-lines"></span>
				</span>
			</div>

			<div class="top-nav-bar top-nav-bar-2">
				<div class="container">
					<nav>
						<ul class="main-nav">
							<li><a href="#home">Inicio</a></li>
							<li><a href="#schedule" class="on">Conferencias</a></li>
							<li><a href="#workshop" class="on">Talleres</a></li>
							<li><a href="#p-tables" class="on">Precios</a></li>
							<li><a href="#contacto" class="on">Contacto</a></li>
						</ul>
					</nav>
				</div>
			</div>

			<div class="slider-container sticky-menu-trigger">
				<ul class="slider-list owl-carousel">
					<li class="item" style="background-image: url(images/back4.png);">
						<div class="home-slider">
							<div class="home-slider-inner">
								<div class="container text-center" style="display:flex;align-items: center;justify-content: center;">
									<img src="images/logo.png" style="width: 50%;" />
									<!--
									<div class="h3">Discover your potential</div>
									<div class="text-bigger">SISeI 9</div>
									-->
								</div>
							</div>
						</div>
					</li>
					
				</ul>

				<a class="btn btn-primary btn-lg btn-discover triggerToSection" href="#workshop">Discover</a>

				<div class="discover-container-outer" id="discover-box">
					<div class="container">
						<div class="discover-container bg-primary">
							<a class="arrow-down triggerToSection" href="#schedule"><i class="fa fa-angle-down"></i></a>
							<div class="row">
								<div class="col-sm-4">
									<div class="discover-bx">	
										<div class="iconic">
											<i class="fa fa-location-arrow"></i>
										</div>
										<div class="text">
											<h4>Teatro MIA</h4>
											<p>Culiacan Sinaloa</p>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="discover-bx">	
										<div class="iconic">
											<i class="fa fa-calendar"></i>
										</div>
										<div class="text">
											<h4>Octubre</h4>
											<p>Del 27 al 29</p>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="discover-bx">	
										<div class="iconic">
											<i class="fa fa-gears"></i>
										</div>
										<div class="text">
											<h4>13 Conferencias</h4>
											<p>15 Talleres</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

			<div class="main-bar bar-sticking">
				<div class="container">
					<div class="logo">
						<a href="/"><img src="images/logo.png" width="5%" style="position: absolute;"></a>
					</div>
					<div class="main-nav-bar">
						<nav class="navbar-collapse collapse">
							<ul class="main-nav">
								<li><a href="#home" class="on">Inicio</a></li>
								<li><a href="#schedule" class="on">Conferencias</a></li>
								<li><a href="#workshop" class="on">Talleres</a></li>
								<li><a href="#p-tables" class="on">Precios</a></li>
								<li><a href="#contacto" class="on">Contacto</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>

		<section id="schedule" class="section">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="schedule-info">
							<h3>CONFERENCIAS</h3>
							<p>&nbsp;</p>
							<!--
							<ul class="list-1 text-primary">
								<li>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie.</li>
								<li>Illum dolore eu feugiat nulla facilisis at vero eros et accumsan.</li>
								<li>Et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis.</li>
							</ul>
						-->
					</div>
					<!-- /schedule-info -->
				</div>
				<div class="col-sm-6">

					<div role="tabpanel">
						<ul class="nav nav-pills" role="tablist">
							<li class="active"><a href="#day-1" data-toggle="tab">Día 1</a></li>
							<li><a href="#day-2" data-toggle="tab">Día 2</a></li>
							<li><a href="#day-3" data-toggle="tab">Día 3</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane fade in active" id="day-1">
								<div class="panel-group accordion-1">
									<div class="panel panel-default "><!-- panel-active -->
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent=".accordion-1" href="#collapse-1">
													<span class="badge">-</span> Inauguración
												</a>
											</h4>
										</div>
											<!--
											<div id="collapse-1" class="panel-collapse collapse in">
												<div class="panel-body">
													Inauguracion
												</div>
											</div>
											-->
									</div>

									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent=".accordion-1" href="#collapse-2">
													<span class="badge">-</span> La mente humana y Usabilidad	
												</a>
											</h4>
										</div>
											<div id="collapse-2" class="panel-collapse collapse">
												<div class="panel-body">
												Bien se conoce que la mente humana es un espacio abierto a grandes posibilidades, un espacio que ni el mayor de los filósofos o el más experimentado científico ha podido descifrar y comprender. Pues bien en esta conferencia nos adentraremos un paso más para la utilidad de esta, aprender a manejar y explotar todos los maravillosos recursos que en ella se encuentran. Dejémonos de ser unas mentes débiles y comencemos a saber usar nuestra mente en los diferentes contextos de la vida.
												<br /><br />
												Por: Francisco Astorga (ClickBalance)
												</div>
											</div>
									</div>

									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent=".accordion-1" href="#collapse-3">
													<span class="badge">-</span> Ruby on Rails	
												</a>
											</h4>
										</div>
											
											<div id="collapse-3" class="panel-collapse collapse">
												<div class="panel-body">
													Basándose en un gran predecesor que es Ruby (un lenguaje orientado a objetos multiplataforma), de fácil manejo y con parecidos con otros lenguajes, llega este framework Ruby on rails (RoR). Con la opinión de muchos desarrolladores, este framework tiene muchas ventajas y facilidades al momento de la creación de grandes aplicaciones web. Al momento de desarrollar tu aplicación web con la ventaja de poder configurar fácilmente y dar lógica con visualizaciones, conexión sencilla a una base de datos, podrás explotar totalmente este entorno. Sigue el lema que te permite escribir un buen código sin la necesidad de repeticiones: <br /><strong>Don’t Repeat yourself</strong><br />Y anímate a conocer al ayudante para crear diversas aplicaciones importantes de la persona de ahora, como la ya muy famosa red social Twitter.
													<br /><br />
													Por: Mike Nieva (Platzi)
												</div>
											</div>
										
									</div>

									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent=".accordion-1" href="#collapse-3">
													<span class="badge">-</span> LiFi y VLC: Uso de luz visible para comunicaciones digitales	
												</a>
											</h4>
										</div>
											
											<div id="collapse-3" class="panel-collapse collapse">
												<div class="panel-body">
													Alguien alguna vez dudó que pudiéramos comunicarnos a largas distancia sin la necesidad del lápiz, papel y la correspondencia, y se creó el telefono y los cables, ,muchos dudaron de juntar el telefono, la televisión y una maquina de escribir y existió las computadoras e Internet, muchos dudaron que no se podría comunicar si no se tenía un cable de por medio, que no podría ser portatil la información y se crearon los celulares, las laptops y el Wi-Fi.<br /><br />Ahora te digo, ¿qué tan dudable es que te comuniques a base de luz y más rápido que el ya conocido y respetado Wi-Fi?  Digo para eso están las lámparas y dispositivos receptores, y se llama Li-Fi.
													<br /><br />
													Por: Carlos René Michel Morales (Lize Oledcomm)
												</div>
											</div>
									</div>

									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent=".accordion-1" href="#collapse-3">
													<span class="badge">-</span> Tecnologías Emergentes, Hardware y Software Libre
												</a>
											</h4>
										</div>
											
											<div id="collapse-3" class="panel-collapse collapse">
												<div class="panel-body">
													Hardware libre como tecnología " emergente " . Actualmente sabemos que hablar de software libre es algo bastante común hoy en día , sin embargo por el contrario muchos aún desconocen la existencia del hardware libre y todo lo referente a ello.
													<br /><br />
													Por: Mónica Hernández (Hacker Girl)
												</div>
											</div>
									</div>

									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent=".accordion-1" href="#collapse-3">
													<span class="badge">-</span> Realidad aumentada en la actualidad
												</a>
											</h4>
										</div>
											
											<div id="collapse-3" class="panel-collapse collapse">
												<div class="panel-body">
													Se usa para definir una visión a través de un dispositivo tecnológico, directa o indirecta, de un entorno físico del mundo real, cuyos elementos se combinan con elementos virtuales para la creación de una realidad mixta en tiempo real. 
													<br /><br />
													Por: Omar López Chávez
												</div>
											</div>
										
									</div>

								</div>
							</div>
							<div class="tab-pane fade" id="day-2">
								<div class="panel-group accordion-1">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent=".accordion-1" href="#day2-collapse-1">
													<span class="badge">-</span> Bitcoins
												</a>
											</h4>
										</div>
											
											<div id="day2-collapse-1" class="panel-collapse collapse">
												<div class="panel-body">
													A modo de emplear las nuevas tecnologías que se desarrollan día con día, los diferentes consumidores y negocios buscan explotarlas hasta el mínimo uso, por ello los negocios a modo de venta y para comodidad del consumidor emplean este desarrollado proyecto llamado bitcoin. Con el funcionamiento de la tecnología peer-to-peer, sin la necesidad de la interferencia de los bancos, y de forma colectiva, bitcoin se encamina a ser un medio rápido y efectivo de pago para las grandes y pequeñas empresas que se abren paso en este recorrido de la red inmensa de comunicación que es Internet.
													<br /><br />
													Por: Brian Nava
												</div>
											</div>
										
									</div>

									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent=".accordion-1" href="#day2-collapse-2">
													<span class="badge">-</span> Videojuegos: Mulaka, pueblos originarios.	
												</a>
											</h4>
										</div>
											
											<div id="day2-collapse-2" class="panel-collapse collapse in">
												<div class="panel-body">
													Si bien como medio de diversión, distracción o entretenimiento los videojuegos han llegado a ser una parte importante de nuestra vida. Desde el básico Tetris hasta juegos muchísimo más elaborados, con historia y gráficos que te hacen sentir dentro de la misma, los juegos obtuvieron una gran expansión dándole la oportunidad a muchos visionarios para dar vida a sueño. Útiles para cualquier plataforma o medio, llegan hasta los rincones más alejados del mundo y unen a un sin fin de personas. ¿Desearias tu unirte a esta historia y ver que hay tras bambalinas de este universo llamado “videojuegos”?
													<br /><br />
													Por: Adolfo Rico y Ever Alan Marquez (LienzoMx)
												</div>
											</div>
										
									</div>

									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent=".accordion-1" href="#day2-collapse-3">
													<span class="badge">-</span> 12 Claves del emprendedor exitoso	
												</a>
											</h4>
										</div>
											
											<div id="day2-collapse-3" class="panel-collapse collapse">
												<div class="panel-body">
													Mucha gente habla hoy en día de ser emprendedor: familia, amigos, escuela, gobierno, en si, la sociedad misma nos exige que seamos emprendedores. Crecer, crear, desarrollar, implementar, seguir creciendo, para así poder lograr ser alguien en la vida y no solo alguien si no el mejor. Pero, ¿alguna vez nos han dicho cómo hacer esto?, ¿una guia, manual, o sugerencia? Pues bien aquí les presentaremos 12 sencillas y magníficas claves para ser aquel que llegue un paso más allá de los demás, descubrir nuestro potencial e implementar en el mañana lo que aprendemos hoy, para poder llegar a ser con un gran éxito  EL emprendedor.
													<br /><br />
													Por: Jesús Topete
												</div>
											</div>
										
									</div>

									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent=".accordion-1" href="#day2-collapse-3">
													<span class="badge">-</span> ¿Que pasaria si todos aprendieramos a programar?	
												</a>
											</h4>
										</div>
											
											<div id="day2-collapse-3" class="panel-collapse collapse">
												<div class="panel-body">
													<br /><br />
													Por: Uriel Hernandez (Codigofacilito)
												</div>
											</div>
										
									</div>

									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent=".accordion-1" href="#day2-collapse-3">
													<span class="badge">-</span> Pensamiento Crítico e Innovación en Educación superior en México
												</a>
											</h4>
										</div>
											
											<div id="day2-collapse-3" class="panel-collapse collapse">
												<div class="panel-body">
													<br /><br />
													Por: Alessio Hagen (Dell Latinoamérica)
												</div>
											</div>
										
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="day-3">
								<div class="panel-group accordion-1">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent=".accordion-1" href="#day3-collapse-1">
													<span class="badge">-</span>Internet de las Cosas
												</a>
											</h4>
										</div>
											
											<div id="day3-collapse-1" class="panel-collapse collapse in">
												<div class="panel-body">
													<br /><br />
													Por: José Carlos Castro Padilla y Omar Palomino Sandoval  (Hunabsys)
												</div>
											</div>
										
									</div>

									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent=".accordion-1" href="#day3-collapse-2">
													<span class="badge">-</span> Manejo de datos grandes	
												</a>
											</h4>
										</div>
											
											<div id="day3-collapse-2" class="panel-collapse collapse">
												<div class="panel-body">
													Hoy en día el mundo va creciendo y su población aumentando, con ello la información que se recauda es todavía mayor, mucha más de lo que las formas tradicionales de almacenamiento pueden administrar, por ello es útil recurrir una vez más al avance de las tecnologías para poder utilizar y resguardar toda clase de información. Así que aquí encontramos la forma de manejar estos grandes datos, y no solo en la cantidad  de información que hay si no en la vasta variedad que pueden existir de datos, aprender la forma de que los diferentes dispositivos y usuarios de hoy en día puedan acceder fácil y velozmente.
													<br /><br />
													Por: Francisco Mekler
												</div>
											</div>
										
									</div>

									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent=".accordion-1" href="#day3-collapse-3">
													<span class="badge">-</span> Sobrevive Creativo	
												</a>
											</h4>
										</div>
											
											<div id="day3-collapse-3" class="panel-collapse collapse">
												<div class="panel-body">
													<br /><br />
													Por: Gerardo García (Fat Panda)
												</div>
											</div>
										
									</div>

									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent=".accordion-1" href="#day3-collapse-3">
													<span class="badge">-</span> Hackeando tu mente con Arduino
												</a>
											</h4>
										</div>
											
											<div id="day3-collapse-3" class="panel-collapse collapse">
												<div class="panel-body">
													Un proyecto de hardware libre algo reciente y ya bastante popular, que ha logrado colarse en los proyectos de varios usuario de estás tecnologías es Arduino. Empezando por ser un proyecto de universidad, este hardware a avanzado  hasta posicionarse en uno de los mejores lugares, si lo que te interesa son los proyectos de electronica y lo microprocesadores.<br />Así que dejemos que Arduino haga su trabajo, se adentre en nuestros conocimientos y hackee nuestra mente.
													<br /><br />
													Por: Andrés Sabas
												</div>
											</div>
										
									</div>

									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent=".accordion-1" href="#day3-collapse-3">
													<span class="badge">-</span> Drones
												</a>
											</h4>
										</div>
											
											<div id="day3-collapse-3" class="panel-collapse collapse">
												<div class="panel-body">
													Los famosos Drones, estos nuevos juguetes del hombre no hacen más que seguir llamando la atención. Si en un principio algo lejano fueron los prototipos creados únicamente para su uso militar, y que aún sean de gran uso en esta rama; no se puede negar el gran auge que han tenido en esta nueva generación, la cual sedienta de las tecnologías utilizan estas herramientas para uso personales. Siendo guiados remotamente o programando su ruta para desplazarse, no cabe duda que son de gran utilidad para llegar a lugares que una persona no puede llegar. Descubramos el funcionamiento de estas maravillosas creaciones del hombre y observemos una fabulosa demostración de su gran capacidad.
													<br /><br />
													Por:  Aarón Aragón (f403)
												</div>
											</div>
									
									</div>

									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent=".accordion-1" href="#day3-collapse-3">
													<span class="badge">-</span> Clausura
												</a>
											</h4>
										</div>
											<!--
											<div id="day3-collapse-3" class="panel-collapse collapse">
												<div class="panel-body">
													Clausura
												</div>
											</div>
											-->
									</div>
								</div>
							</div>
						</div><!-- /tab-content -->
					</div>

				</div>
			</div>
		</div>
	</section>

	<section id="workshop">
		<div class="section section-parallax bg-primary no-padding-bottom" style="background-image:url(images/resource/bg-1.png);">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<h3>TALLERES</h3>
						<p>&nbsp;</p>
						<ul class="workshop-nav">
							<li class="active">
								<a href="#workshop-1" data-toggle="tab">Diseño y Programación de Videojuegos</a>
							</li>
							<li><a href="#workshop-2" data-toggle="tab">Web Scraping</a></li>
							<li><a href="#workshop-3" data-toggle="tab">Hardware</a></li>
							<li><a href="#workshop-4" data-toggle="tab">Git</a></li>
							<li><a href="#workshop-5" data-toggle="tab">Ruby on Rails</a></li>
							<li><a href="#workshop-6" data-toggle="tab">Arduino</a></li>
							<li><a href="#workshop-7" data-toggle="tab">Javascript</a></li>
							<li><a href="#workshop-8" data-toggle="tab">Titanium</a></li>
							<li><a href="#workshop-9" data-toggle="tab">Telefonia IP</a></li>
							<li><a href="#workshop-10" data-toggle="tab">iOS</a></li>
							<li><a href="#workshop-11" data-toggle="tab">Android</a></li>
							<li><a href="#workshop-12" data-toggle="tab">Internet de las cosas</a></li>
							<li><a href="#workshop-13" data-toggle="tab">HTML5</a></li>
							<li><a href="#workshop-14" data-toggle="tab">Marketing Digital</a></li>
							<li><a href="#workshop-15" data-toggle="tab">Unity</a></li>
						</ul>
					</div>
					<div class="col-md-6">

						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade in active" id="workshop-1">
								<div class="thumbnail">
									<figure class="image">
										<img src="images/talleres/juegos.png" alt="image">
									</figure>
									<div class="caption">
										<h4>Diseño y Programación de Videojuegos</h4>
										<p>&nbsp;</p>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="workshop-2">
								<div class="thumbnail">
									<figure class="image">
										<img src="images/talleres/web-scraping.png" alt="image">
									</figure>
									<div class="caption">
										<h4>Web Scraping</h4>
										<p>&nbsp;</p>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="workshop-3">
								<div class="thumbnail">
									<figure class="image">
										<img src="images/talleres/hardware.png" alt="image">
									</figure>
									<div class="caption">
										<h4>Hardware</h4>
										<p>&nbsp;</p>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="workshop-4">
								<div class="thumbnail">
									<figure class="image">
										<img src="images/talleres/git.png" alt="image">
									</figure>
									<div class="caption">
										<h4>GIT</h4>
										<p>&nbsp;</p>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="workshop-5">
								<div class="thumbnail">
									<figure class="image">
										<img src="images/talleres/ruby.png" alt="image" style="width:300px; margin-left:10px;">
									</figure>
									<div class="caption">
										<h4>Ruby on Rails</h4>
										<p>&nbsp;</p>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="workshop-6">
								<div class="thumbnail">
									<figure class="image">
										<img src="images/talleres/arduino.png" alt="image">
									</figure>
									<div class="caption">
										<h4>Arduino</h4>
										<p>&nbsp;</p>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="workshop-7">
								<div class="thumbnail">
									<figure class="image">
										<img src="images/talleres/js.png" alt="image">
									</figure>
									<div class="caption">
										<h4>Javascript</h4>
										<p>&nbsp;</p>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="workshop-8">
								<div class="thumbnail">
									<figure class="image">
										<img src="images/talleres/titanium.png" alt="image">
									</figure>
									<div class="caption">
										<h4>Titanium</h4>
										<p>&nbsp;</p>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="workshop-9">
								<div class="thumbnail">
									<figure class="image">
										<img src="images/talleres/telefonia.png" alt="image">
									</figure>
									<div class="caption">
										<h4>Telefonia IP</h4>
										<p>&nbsp;</p>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="workshop-10">
								<div class="thumbnail">
									<figure class="image">
										<img src="images/talleres/ios.png" alt="image">
									</figure>
									<div class="caption">
										<h4>iOS</h4>
										<p>&nbsp;</p>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="workshop-11">
								<div class="thumbnail">
									<figure class="image">
										<img src="images/talleres/android.png" alt="image">
									</figure>
									<div class="caption">
										<h4>Android</h4>
										<p>&nbsp;</p>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="workshop-12">
								<div class="thumbnail">
									<figure class="image">
										<img src="images/talleres/internetCosas.png" alt="image">
									</figure>
									<div class="caption">
										<h4>Internet de las cosas</h4>
										<p>&nbsp;</p>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="workshop-13">
								<div class="thumbnail">
									<figure class="image">
										<img src="images/talleres/html5.png" alt="image">
									</figure>
									<div class="caption">
										<h4>HTML5</h4>
										<p>&nbsp;</p>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="workshop-14">
								<div class="thumbnail">
									<figure class="image">
										<img src="images/talleres/marketing.png" alt="image">
									</figure>
									<div class="caption">
										<h4>Marketing Digital</h4>
										<p>&nbsp;</p>
									</div>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="workshop-15">
								<div class="thumbnail">
									<figure class="image">
										<img src="images/talleres/unity.png" alt="image">
									</figure>
									<div class="caption">
										<h4>Unity</h4>
										<p>&nbsp;</p>
									</div>
								</div>
							</div>

						</div><!-- /tab-content -->
					</div>
				</div>
			</div>
		</div>

		<div class="slider">
			<div id="sync1" class="owl-carousel">


				<div class="item" style="height: 300px; background: #000;">
					<!--
					<span class="video-button-play"><i class="fa fa-play-circle"></i></span>
					<span class="video-button-pause"><i class="fa fa-pause"></i></span>
					
					<video class="parallax-video" src="video.mp4" style="z-index:0; height:300px;">
					</video>
					-->
					<iframe  class="parallax-video" type="text/html" height="300" src="http://www.youtube.com/embed/J9cvwJAqWJQ" frameborder="0" style="z-index:0; height:300px; width: 100%;"></iframe>
				</div>

			</div>

			<div class="container">
				<div id="sync2" class="owl-carousel">


				</div>
			</div>
		</div><!-- /slider -->
	</section>




	<section id="p-tables">
		<div class="section bg-light-grey">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-4">
						<div class="pricing-table-box" data-package="regular">
							<div class="pricing-stars">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</div>
							<div class="row">
								<div class="col-xs-4 col-sm-12 col-md-4">
									<div class="right-part">
										<h4>Basico</h4>
										<span class="table-price">$1,100</span>
									</div>
								</div>
								<div class="col-xs-8 col-sm-12 col-md-8">
									<ul class="pricing-list">
										<li class="feature">3 dias de conferencia</li>
										<li>1 kit de bienvenida</li>
										<li>1 taller a elegir*</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="pricing-table-box selected" data-package="pro">
							<div class="pricing-stars">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</div>
							<div class="row">
								<div class="col-xs-4 col-sm-12 col-md-4">
									<div class="right-part">
										<h4>Foraneo</h4>
										<span class="table-price">$2,500</span>
									</div>
								</div>
								<div class="col-xs-8 col-sm-12 col-md-8">
									<ul class="pricing-list">
										<li class="feature">3 dias de conferencia</li>
										<li>1 kit de bienvenida</li>
										<li>1 taller a elegir*</li>
										<li class="feature">Viaje a Mazatlán</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="pricing-table-box" data-package="ultimate">
							<div class="pricing-stars">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
							</div>
							<div class="row">
								<div class="col-xs-4 col-sm-12 col-md-4">
									<div class="right-part">
										<h4>Completo</h4>
										<span class="table-price">$1,500</span>
									</div>
								</div>
								<div class="col-xs-8 col-sm-12 col-md-8">
									<ul class="pricing-list">
										<li class="feature">3 dias de conferencia</li>
										<li>1 kit de bienvenida</li>
										<li>1 taller a elegir*</li>
										<li class="feature">Viaje a Mazatlán</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<p>&nbsp;</p>
	<section id="contacto">
		<div class="container">
			<div class="row">
			<div class="col-sm-4">
					<div class="schedule-info">
						<h3>CONTACTO</h3>
						<p>&nbsp;</p>
					</div>
					<!-- /schedule-info -->
				</div>
				<div class="col-sm-8">
					

					<form class="form-horizontal" role="form" method="post">
						<div class="form-group">
							<label for="name" class="col-sm-2 control-label">Nombre</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="name" name="name" required>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">Correo</label>
							<div class="col-sm-10">
								<input type="email" class="form-control" id="email" name="email" required>
							</div>
						</div>
						<div class="form-group">
							<label for="message" class="col-sm-2 control-label">Mensaje</label>
							<div class="col-sm-10">
								<textarea class="form-control" rows="4" name="message" required></textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="message" class="col-sm-2 control-label">&nbsp;</label>
							<div class="col-sm-10">
								<div class="g-recaptcha" data-sitekey="6LeX2gwTAAAAAA62lpZVgru2k9Dg7PbDK-dXokWv"></div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-10 col-sm-offset-2">
								<input type="submit" value="Enviar" class="btn btn-lg btn-success">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-10 col-sm-offset-2">
							</div>
						</div>
					</form>
				</div>
			</div>
		</section>

		<footer class="site-footer">
			<div class="container">
				<p>2015 © <a href="#">SISeI</a>. All rights reserved.</p>
				<a href="#" class="settings"><i class="fa fa-gear"></i></a>
				<ul class="social-icons">
					<li>
						<a href="https://www.facebook.com/Simposio.SISeI?fref=ts">
							<i class="fa fa-facebook-square"></i>
						</a>
					</li>
					<li>
						<a href="https://twitter.com/SiseiOnline">
							<i class="fa fa-twitter-square"></i>
						</a>
					</li>
					<li><a href="#"><i class="fa fa-youtube-square"></i></a></li>
					<li><a href="#"><i class="fa fa-instagram"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
				</ul>
			</div>
		</footer>

	</div>

	<script src="js/jquery-1.11.2.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>	
	<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="js/script.js"></script>
	<script src="js/ie10-viewport-bug-workaround.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
</body>
</html>